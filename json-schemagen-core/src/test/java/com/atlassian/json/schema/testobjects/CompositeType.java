package com.atlassian.json.schema.testobjects;

public enum CompositeType
{
    AND, OR
}
